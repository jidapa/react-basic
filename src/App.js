import React from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import ListUser from './components/ListUser';
import Sidebar from './components/Sidebar';

function App() {
  const Mytitle = 'My Title';
  return (
    <div className="App">
      <Header title={Mytitle} age={100} />
      <Sidebar name={Mytitle} age={100} />
      <ListUser />
      <Footer />
    </div>
  );
}

export default App;
