//rafcp
import React from 'react'
import PropTypes from 'prop-types'

const Sidebar = props => {
    return (
        <>
            <h2>{props.name}</h2>
            <p>{props.age}</p>
        </>
    )
}

Sidebar.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number
}

export default Sidebar
