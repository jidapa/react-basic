import React, { Component } from 'react'

export default class Footer extends Component {
    state = {
        name: 'Mary'
    }

    go = () => {
        this.setState({
            name: 'Jonh'
        })
    }
    
    render() {
        return (
            <div>
                <hr />
                <button onClick={this.go}>Click Me</button>
                <p>
                    {
                        this.state.name && (
                            <span>Hello {this.state.name} </span>
                        )
                    }
                    &#9400; {new Date().getFullYear()} </p>
            </div>
        )
    }
}
