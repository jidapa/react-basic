import React from 'react'

const ListUser = () => {
    const users = [
        { id: 1, name: 'John' },
        { id: 2, name: 'Mary' }
    ];
    return (
        <div>
            <>
                <table border="1">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            users.map((user, index) => { //index ไม่ใช้ก็ได้ เพราะเป็น optional
                                return (
                                    <tr key={user.id}>
                                        <td>{user.id}</td>
                                        <td>{user.name}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </>
        </div>
    )
}

export default ListUser
