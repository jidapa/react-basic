import React from 'react'

const Header = ({title, age}) => { // desturing props
    // const { title,age } = props; //destructuring
    const info = <h1>Hello jida</h1>;
    const go = (title, e) =>{
        alert(title)
        e.preventDefault();
    }
    const myTitle = title;
    const version = '16.9.0';
    return (
        <>
            <h1 onClick={(e) => go('React ' + version , e)}>MongoDB</h1>
            {info}
            {myTitle}
            {age}
        </>
    )
}

export default Header
